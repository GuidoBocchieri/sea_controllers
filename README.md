# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository contains matlab-generated source code for kippc and akippc controllers
* kippc is slower and "naive" (just kalman filter, pole placement and integral action)
* akippc has same structure but contains augmented kalman observer, feedforward disturbance compensation and is tuned "more aggressive"
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact