#include "slros_initialize.h"

ros::NodeHandle * SLROSNodePtr;
const std::string SLROSNodeName = "SEA_Controller_AKIPPC";

// For Block SEA_Controller_AKIPPC/Input Interface/Subscribe
SimulinkSubscriber<sensor_msgs::JointState, SL_Bus_SEA_Controller_AKIPPC_sensor_msgs_JointState> Sub_SEA_Controller_AKIPPC_14;

// For Block SEA_Controller_AKIPPC/Input Interface/Subscribe1
SimulinkSubscriber<std_msgs::Float32, SL_Bus_SEA_Controller_AKIPPC_std_msgs_Float32> Sub_SEA_Controller_AKIPPC_35;

// For Block SEA_Controller_AKIPPC/Output Interface/Publish
SimulinkPublisher<sensor_msgs::JointState, SL_Bus_SEA_Controller_AKIPPC_sensor_msgs_JointState> Pub_SEA_Controller_AKIPPC_15;

void slros_node_init(int argc, char** argv)
{
  ros::init(argc, argv, SLROSNodeName);
  SLROSNodePtr = new ros::NodeHandle();
}

