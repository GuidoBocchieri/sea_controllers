//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
//
// File: SEA_Controller_AKIPPC.cpp
//
// Code generated for Simulink model 'SEA_Controller_AKIPPC'.
//
// Model version                  : 1.15
// Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
// C/C++ source code generated on : Fri Sep  1 13:03:41 2017
//
// Target selection: ert.tlc
// Embedded hardware selection: Generic->Unspecified (assume 32-bit Generic)
// Code generation objectives: Unspecified
// Validation result: Not run
//
#include "SEA_Controller_AKIPPC.h"
#include "SEA_Controller_AKIPPC_private.h"
#define SEA_Controller__MessageQueueLen (1)

// Block signals (auto storage)
B_SEA_Controller_AKIPPC_T SEA_Controller_AKIPPC_B;

// Continuous states
X_SEA_Controller_AKIPPC_T SEA_Controller_AKIPPC_X;

// Block states (auto storage)
DW_SEA_Controller_AKIPPC_T SEA_Controller_AKIPPC_DW;

// Real-time model
RT_MODEL_SEA_Controller_AKIPP_T SEA_Controller_AKIPPC_M_;
RT_MODEL_SEA_Controller_AKIPP_T *const SEA_Controller_AKIPPC_M =
  &SEA_Controller_AKIPPC_M_;

//
// This function updates continuous states using the ODE3 fixed-step
// solver algorithm
//
static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si )
{
  // Solver Matrices
  static const real_T rt_ODE3_A[3] = {
    1.0/2.0, 3.0/4.0, 1.0
  };

  static const real_T rt_ODE3_B[3][3] = {
    { 1.0/2.0, 0.0, 0.0 },

    { 0.0, 3.0/4.0, 0.0 },

    { 2.0/9.0, 1.0/3.0, 4.0/9.0 }
  };

  time_T t = rtsiGetT(si);
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE3_IntgData *id = (ODE3_IntgData *)rtsiGetSolverData(si);
  real_T *y = id->y;
  real_T *f0 = id->f[0];
  real_T *f1 = id->f[1];
  real_T *f2 = id->f[2];
  real_T hB[3];
  int_T i;
  int_T nXc = 7;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);

  // Save the state values at time t in y, we'll use x as ynew.
  (void) memcpy(y, x,
                (uint_T)nXc*sizeof(real_T));

  // Assumes that rtsiSetT and ModelOutputs are up-to-date
  // f0 = f(t,y)
  rtsiSetdX(si, f0);
  SEA_Controller_AKIPPC_derivatives();

  // f(:,2) = feval(odefile, t + hA(1), y + f*hB(:,1), args(:)(*));
  hB[0] = h * rt_ODE3_B[0][0];
  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0]);
  }

  rtsiSetT(si, t + h*rt_ODE3_A[0]);
  rtsiSetdX(si, f1);
  SEA_Controller_AKIPPC_step();
  SEA_Controller_AKIPPC_derivatives();

  // f(:,3) = feval(odefile, t + hA(2), y + f*hB(:,2), args(:)(*));
  for (i = 0; i <= 1; i++) {
    hB[i] = h * rt_ODE3_B[1][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1]);
  }

  rtsiSetT(si, t + h*rt_ODE3_A[1]);
  rtsiSetdX(si, f2);
  SEA_Controller_AKIPPC_step();
  SEA_Controller_AKIPPC_derivatives();

  // tnew = t + hA(3);
  // ynew = y + f*hB(:,3);
  for (i = 0; i <= 2; i++) {
    hB[i] = h * rt_ODE3_B[2][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2]);
  }

  rtsiSetT(si, tnew);
  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

// Model step function
void SEA_Controller_AKIPPC_step(void)
{
  SL_Bus_SEA_Controller_AKIPPC_std_msgs_Float32 varargout_2;
  boolean_T varargout_1;
  real_T rtb_Gain1;
  int32_T i;
  real_T rtb_taum_0;
  real_T tmp[2];
  real_T tmp_0[2];
  int32_T i_0;
  if (rtmIsMajorTimeStep(SEA_Controller_AKIPPC_M)) {
    // set solver stop time
    rtsiSetSolverStopTime(&SEA_Controller_AKIPPC_M->solverInfo,
                          ((SEA_Controller_AKIPPC_M->Timing.clockTick0+1)*
      SEA_Controller_AKIPPC_M->Timing.stepSize0));
  }                                    // end MajorTimeStep

  // Update absolute time of base rate at minor time step
  if (rtmIsMinorTimeStep(SEA_Controller_AKIPPC_M)) {
    SEA_Controller_AKIPPC_M->Timing.t[0] = rtsiGetT
      (&SEA_Controller_AKIPPC_M->solverInfo);
  }

  if (rtmIsMajorTimeStep(SEA_Controller_AKIPPC_M)) {
    // Constant: '<S6>/X0'
    for (i = 0; i < 6; i++) {
      SEA_Controller_AKIPPC_B.X0[i] = SEA_Controller_AKIPPC_P.X0_Value[i];
    }

    // End of Constant: '<S6>/X0'

    // Constant: '<S4>/Constant'
    SEA_Controller_AKIPPC_B.Constant = SEA_Controller_AKIPPC_P.Constant_Value_j;
  }

  // Integrator: '<S6>/MemoryX'
  if (SEA_Controller_AKIPPC_DW.MemoryX_IWORK != 0) {
    for (i = 0; i < 6; i++) {
      SEA_Controller_AKIPPC_X.MemoryX_CSTATE[i] = SEA_Controller_AKIPPC_B.X0[i];
    }
  }

  // Integrator: '<S4>/Integrator'
  if (SEA_Controller_AKIPPC_DW.Integrator_IWORK != 0) {
    SEA_Controller_AKIPPC_X.Integrator_CSTATE = SEA_Controller_AKIPPC_B.Constant;
  }

  // SignalConversion: '<S4>/TmpSignal ConversionAtGainInport1' incorporates:
  //   Gain: '<S4>/Gain'
  //   Integrator: '<S4>/Integrator'
  //   Integrator: '<S6>/MemoryX'

  SEA_Controller_AKIPPC_B.dv1[0] = SEA_Controller_AKIPPC_X.MemoryX_CSTATE[0];
  SEA_Controller_AKIPPC_B.dv1[1] = SEA_Controller_AKIPPC_X.MemoryX_CSTATE[1];
  SEA_Controller_AKIPPC_B.dv1[2] = SEA_Controller_AKIPPC_X.MemoryX_CSTATE[2];
  SEA_Controller_AKIPPC_B.dv1[3] = SEA_Controller_AKIPPC_X.MemoryX_CSTATE[3];
  SEA_Controller_AKIPPC_B.dv1[4] = SEA_Controller_AKIPPC_X.Integrator_CSTATE;

  // Gain: '<S4>/Gain'
  rtb_taum_0 = 0.0;
  for (i = 0; i < 5; i++) {
    rtb_taum_0 += SEA_Controller_AKIPPC_P.gain[i] *
      SEA_Controller_AKIPPC_B.dv1[i];
  }

  // MATLAB Function: '<S3>/Assign' incorporates:
  //   Constant: '<S58>/Constant'
  //   Gain: '<S4>/Gain'
  //   Integrator: '<S6>/MemoryX'
  //   Sum: '<S4>/Sum1'

  SEA_Controller_AKIPPC_B.msg = SEA_Controller_AKIPPC_P.Constant_Value_h;

  //  To set datatype of MSG output:
  //   1. Create buses for this model: robotics.ros.createSimulinkBus(gcs)
  //   1. Click "Edit Data" in  Toolstrip to open "Ports and Data Manager"
  //   2. Select MSG and set datatype to "Bus: SL_Bus_<modelname>_<messageType>" 
  //  See:
  //  http://www.mathworks.com/help/simulink/ug/create-structures-in-matlab-function-blocks.html 
  // MATLAB Function 'Output Interface/Assign': '<S57>:1'
  //  blankMessage (sensor_msgs/LaserScan) consists of
  //              Header: [1x1 Header]
  //            AngleMin: 0
  //            AngleMax: 0
  //      AngleIncrement: 0
  //       TimeIncrement: 0
  //            ScanTime: 0
  //            RangeMin: 0
  //            RangeMax: 0
  //              Ranges: [0x1 single]
  //         Intensities: [0x1 single]
  //  Set Ranges array to have length 10, and modify indices 1, 3, 5 and 7
  // '<S57>:1:24' blankMessage.Effort_SL_Info.CurrentLength = uint32(1);
  SEA_Controller_AKIPPC_B.msg.Effort_SL_Info.CurrentLength = 1U;

  // '<S57>:1:25' blankMessage.Effort([1]) = u;
  SEA_Controller_AKIPPC_B.msg.Effort[0] =
    SEA_Controller_AKIPPC_X.MemoryX_CSTATE[4] + rtb_taum_0;

  // Outputs for Atomic SubSystem: '<S3>/Publish'
  // Start for MATLABSystem: '<S59>/SinkBlock' incorporates:
  //   MATLABSystem: '<S59>/SinkBlock'

  // '<S57>:1:27' msg = blankMessage;
  Pub_SEA_Controller_AKIPPC_15.publish(&SEA_Controller_AKIPPC_B.msg);

  // End of Outputs for SubSystem: '<S3>/Publish'
  if (rtmIsMajorTimeStep(SEA_Controller_AKIPPC_M)) {
    // Outputs for Atomic SubSystem: '<S2>/Subscribe1'
    // Start for MATLABSystem: '<S54>/SourceBlock' incorporates:
    //   MATLABSystem: '<S54>/SourceBlock'

    varargout_1 = Sub_SEA_Controller_AKIPPC_35.getLatestMessage(&varargout_2);

    // Outputs for Enabled SubSystem: '<S2>/Reference sensing' incorporates:
    //   EnablePort: '<S52>/Enable'

    // Outputs for Enabled SubSystem: '<S54>/Enabled Subsystem' incorporates:
    //   EnablePort: '<S56>/Enable'

    if (varargout_1) {
      // DataTypeConversion: '<S52>/Data Type Conversion'
      SEA_Controller_AKIPPC_B.DataTypeConversion = varargout_2.Data;
    }

    // End of Start for MATLABSystem: '<S54>/SourceBlock'
    // End of Outputs for SubSystem: '<S54>/Enabled Subsystem'
    // End of Outputs for SubSystem: '<S2>/Reference sensing'
    // End of Outputs for SubSystem: '<S2>/Subscribe1'

    // Gain: '<S5>/Gain1'
    rtb_Gain1 = SEA_Controller_AKIPPC_P.Gain1_Gain *
      SEA_Controller_AKIPPC_B.DataTypeConversion;

    // Outputs for Atomic SubSystem: '<S2>/Subscribe'
    // Start for MATLABSystem: '<S53>/SourceBlock' incorporates:
    //   MATLABSystem: '<S53>/SourceBlock'

    varargout_1 = Sub_SEA_Controller_AKIPPC_14.getLatestMessage
      (&SEA_Controller_AKIPPC_B.msg);

    // Outputs for Enabled SubSystem: '<S2>/Joint sensing' incorporates:
    //   EnablePort: '<S51>/Enable'

    // Outputs for Enabled SubSystem: '<S53>/Enabled Subsystem' incorporates:
    //   EnablePort: '<S55>/Enable'

    if (varargout_1) {
      // Selector: '<S51>/Selector1' incorporates:
      //   Constant: '<S51>/Constant2'

      SEA_Controller_AKIPPC_B.pos_motor = SEA_Controller_AKIPPC_B.msg.Position
        [(int32_T)SEA_Controller_AKIPPC_P.Constant2_Value - 1];

      // Sum: '<S51>/Sum1' incorporates:
      //   Constant: '<S51>/Constant4'
      //   Selector: '<S51>/Selector3'

      SEA_Controller_AKIPPC_B.Sum1 = SEA_Controller_AKIPPC_B.msg.Position
        [(int32_T)SEA_Controller_AKIPPC_P.Constant4_Value - 1] +
        SEA_Controller_AKIPPC_B.pos_motor;
    }

    // End of Start for MATLABSystem: '<S53>/SourceBlock'
    // End of Outputs for SubSystem: '<S53>/Enabled Subsystem'
    // End of Outputs for SubSystem: '<S2>/Joint sensing'
    // End of Outputs for SubSystem: '<S2>/Subscribe'

    // Outputs for Enabled SubSystem: '<S25>/MeasurementUpdate' incorporates:
    //   EnablePort: '<S50>/Enable'

    if (rtmIsMajorTimeStep(SEA_Controller_AKIPPC_M)) {
      // Constant: '<S6>/Enable'
      if (SEA_Controller_AKIPPC_P.Enable_Value) {
        if (!SEA_Controller_AKIPPC_DW.MeasurementUpdate_MODE) {
          SEA_Controller_AKIPPC_DW.MeasurementUpdate_MODE = true;
        }
      } else {
        if (SEA_Controller_AKIPPC_DW.MeasurementUpdate_MODE) {
          // Disable for Outport: '<S50>/L*(y[k]-yhat[k|k-1])'
          for (i = 0; i < 6; i++) {
            SEA_Controller_AKIPPC_B.Product3[i] =
              SEA_Controller_AKIPPC_P.Lykyhatkk1_Y0;
          }

          // End of Disable for Outport: '<S50>/L*(y[k]-yhat[k|k-1])'
          SEA_Controller_AKIPPC_DW.MeasurementUpdate_MODE = false;
        }
      }

      // End of Constant: '<S6>/Enable'
    }

    // End of Outputs for SubSystem: '<S25>/MeasurementUpdate'
  }

  // Outputs for Enabled SubSystem: '<S25>/MeasurementUpdate' incorporates:
  //   EnablePort: '<S50>/Enable'

  if (SEA_Controller_AKIPPC_DW.MeasurementUpdate_MODE) {
    // Sum: '<S50>/Sum'
    SEA_Controller_AKIPPC_B.dv2[0] = SEA_Controller_AKIPPC_B.pos_motor;
    SEA_Controller_AKIPPC_B.dv2[1] = SEA_Controller_AKIPPC_B.Sum1;
    for (i = 0; i < 2; i++) {
      // Product: '<S50>/C[k]*xhat[k|k-1]' incorporates:
      //   Constant: '<S6>/C'
      //   Integrator: '<S6>/MemoryX'
      //   Sum: '<S50>/Add1'

      tmp[i] = 0.0;
      for (i_0 = 0; i_0 < 6; i_0++) {
        tmp[i] += SEA_Controller_AKIPPC_P.C_Value[(i_0 << 1) + i] *
          SEA_Controller_AKIPPC_X.MemoryX_CSTATE[i_0];
      }

      // End of Product: '<S50>/C[k]*xhat[k|k-1]'

      // Sum: '<S50>/Sum' incorporates:
      //   Constant: '<S6>/D'
      //   Gain: '<S4>/Gain'
      //   Product: '<S50>/D[k]*u[k]'
      //   Product: '<S50>/Product3'
      //   Sum: '<S50>/Add1'

      tmp_0[i] = SEA_Controller_AKIPPC_B.dv2[i] -
        (SEA_Controller_AKIPPC_P.D_Value[i] * rtb_taum_0 + tmp[i]);
    }

    // Product: '<S50>/Product3' incorporates:
    //   Constant: '<S7>/KalmanGainL'

    for (i = 0; i < 6; i++) {
      SEA_Controller_AKIPPC_B.Product3[i] = 0.0;
      SEA_Controller_AKIPPC_B.Product3[i] +=
        SEA_Controller_AKIPPC_P.KalmanGainL_Value[i] * tmp_0[0];
      SEA_Controller_AKIPPC_B.Product3[i] +=
        SEA_Controller_AKIPPC_P.KalmanGainL_Value[i + 6] * tmp_0[1];
    }
  }

  // End of Outputs for SubSystem: '<S25>/MeasurementUpdate'
  for (i = 0; i < 6; i++) {
    // Product: '<S25>/A[k]*xhat[k|k-1]' incorporates:
    //   Constant: '<S6>/A'
    //   Integrator: '<S6>/MemoryX'
    //   Sum: '<S25>/Add'

    SEA_Controller_AKIPPC_B.dv0[i] = 0.0;
    for (i_0 = 0; i_0 < 6; i_0++) {
      SEA_Controller_AKIPPC_B.dv0[i] += SEA_Controller_AKIPPC_P.A_Value[6 * i_0
        + i] * SEA_Controller_AKIPPC_X.MemoryX_CSTATE[i_0];
    }

    // End of Product: '<S25>/A[k]*xhat[k|k-1]'

    // Sum: '<S25>/Add' incorporates:
    //   Constant: '<S6>/B'
    //   Gain: '<S4>/Gain'
    //   Product: '<S25>/B[k]*u[k]'

    SEA_Controller_AKIPPC_B.Add[i] = (SEA_Controller_AKIPPC_P.B_Value[i] *
      rtb_taum_0 + SEA_Controller_AKIPPC_B.dv0[i]) +
      SEA_Controller_AKIPPC_B.Product3[i];
  }

  if (rtmIsMajorTimeStep(SEA_Controller_AKIPPC_M)) {
    // Sum: '<S4>/Sum'
    SEA_Controller_AKIPPC_B.error = SEA_Controller_AKIPPC_B.Sum1 - rtb_Gain1;
  }

  if (rtmIsMajorTimeStep(SEA_Controller_AKIPPC_M)) {
    // Update for Integrator: '<S6>/MemoryX'
    SEA_Controller_AKIPPC_DW.MemoryX_IWORK = 0;

    // Update for Integrator: '<S4>/Integrator'
    SEA_Controller_AKIPPC_DW.Integrator_IWORK = 0;
  }                                    // end MajorTimeStep

  if (rtmIsMajorTimeStep(SEA_Controller_AKIPPC_M)) {
    rt_ertODEUpdateContinuousStates(&SEA_Controller_AKIPPC_M->solverInfo);

    // Update absolute time for base rate
    // The "clockTick0" counts the number of times the code of this task has
    //  been executed. The absolute time is the multiplication of "clockTick0"
    //  and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
    //  overflow during the application lifespan selected.

    ++SEA_Controller_AKIPPC_M->Timing.clockTick0;
    SEA_Controller_AKIPPC_M->Timing.t[0] = rtsiGetSolverStopTime
      (&SEA_Controller_AKIPPC_M->solverInfo);

    {
      // Update absolute timer for sample time: [0.001s, 0.0s]
      // The "clockTick1" counts the number of times the code of this task has
      //  been executed. The resolution of this integer timer is 0.001, which is the step size
      //  of the task. Size of "clockTick1" ensures timer will not overflow during the
      //  application lifespan selected.

      SEA_Controller_AKIPPC_M->Timing.clockTick1++;
    }
  }                                    // end MajorTimeStep
}

// Derivatives for root system: '<Root>'
void SEA_Controller_AKIPPC_derivatives(void)
{
  int32_T i;
  XDot_SEA_Controller_AKIPPC_T *_rtXdot;
  _rtXdot = ((XDot_SEA_Controller_AKIPPC_T *) SEA_Controller_AKIPPC_M->derivs);

  // Derivatives for Integrator: '<S6>/MemoryX'
  for (i = 0; i < 6; i++) {
    _rtXdot->MemoryX_CSTATE[i] = SEA_Controller_AKIPPC_B.Add[i];
  }

  // End of Derivatives for Integrator: '<S6>/MemoryX'

  // Derivatives for Integrator: '<S4>/Integrator'
  _rtXdot->Integrator_CSTATE = SEA_Controller_AKIPPC_B.error;
}

// Model initialize function
void SEA_Controller_AKIPPC_initialize(void)
{
  // Registration code

  // initialize real-time model
  (void) memset((void *)SEA_Controller_AKIPPC_M, 0,
                sizeof(RT_MODEL_SEA_Controller_AKIPP_T));

  {
    // Setup solver object
    rtsiSetSimTimeStepPtr(&SEA_Controller_AKIPPC_M->solverInfo,
                          &SEA_Controller_AKIPPC_M->Timing.simTimeStep);
    rtsiSetTPtr(&SEA_Controller_AKIPPC_M->solverInfo, &rtmGetTPtr
                (SEA_Controller_AKIPPC_M));
    rtsiSetStepSizePtr(&SEA_Controller_AKIPPC_M->solverInfo,
                       &SEA_Controller_AKIPPC_M->Timing.stepSize0);
    rtsiSetdXPtr(&SEA_Controller_AKIPPC_M->solverInfo,
                 &SEA_Controller_AKIPPC_M->derivs);
    rtsiSetContStatesPtr(&SEA_Controller_AKIPPC_M->solverInfo, (real_T **)
                         &SEA_Controller_AKIPPC_M->contStates);
    rtsiSetNumContStatesPtr(&SEA_Controller_AKIPPC_M->solverInfo,
      &SEA_Controller_AKIPPC_M->Sizes.numContStates);
    rtsiSetNumPeriodicContStatesPtr(&SEA_Controller_AKIPPC_M->solverInfo,
      &SEA_Controller_AKIPPC_M->Sizes.numPeriodicContStates);
    rtsiSetPeriodicContStateIndicesPtr(&SEA_Controller_AKIPPC_M->solverInfo,
      &SEA_Controller_AKIPPC_M->periodicContStateIndices);
    rtsiSetPeriodicContStateRangesPtr(&SEA_Controller_AKIPPC_M->solverInfo,
      &SEA_Controller_AKIPPC_M->periodicContStateRanges);
    rtsiSetErrorStatusPtr(&SEA_Controller_AKIPPC_M->solverInfo,
                          (&rtmGetErrorStatus(SEA_Controller_AKIPPC_M)));
    rtsiSetRTModelPtr(&SEA_Controller_AKIPPC_M->solverInfo,
                      SEA_Controller_AKIPPC_M);
  }

  rtsiSetSimTimeStep(&SEA_Controller_AKIPPC_M->solverInfo, MAJOR_TIME_STEP);
  SEA_Controller_AKIPPC_M->intgData.y = SEA_Controller_AKIPPC_M->odeY;
  SEA_Controller_AKIPPC_M->intgData.f[0] = SEA_Controller_AKIPPC_M->odeF[0];
  SEA_Controller_AKIPPC_M->intgData.f[1] = SEA_Controller_AKIPPC_M->odeF[1];
  SEA_Controller_AKIPPC_M->intgData.f[2] = SEA_Controller_AKIPPC_M->odeF[2];
  SEA_Controller_AKIPPC_M->contStates = ((X_SEA_Controller_AKIPPC_T *)
    &SEA_Controller_AKIPPC_X);
  rtsiSetSolverData(&SEA_Controller_AKIPPC_M->solverInfo, (void *)
                    &SEA_Controller_AKIPPC_M->intgData);
  rtsiSetSolverName(&SEA_Controller_AKIPPC_M->solverInfo,"ode3");
  rtmSetTPtr(SEA_Controller_AKIPPC_M, &SEA_Controller_AKIPPC_M->Timing.tArray[0]);
  SEA_Controller_AKIPPC_M->Timing.stepSize0 = 0.001;
  rtmSetFirstInitCond(SEA_Controller_AKIPPC_M, 1);

  // block I/O
  (void) memset(((void *) &SEA_Controller_AKIPPC_B), 0,
                sizeof(B_SEA_Controller_AKIPPC_T));

  // states (continuous)
  {
    (void) memset((void *)&SEA_Controller_AKIPPC_X, 0,
                  sizeof(X_SEA_Controller_AKIPPC_T));
  }

  // states (dwork)
  (void) memset((void *)&SEA_Controller_AKIPPC_DW, 0,
                sizeof(DW_SEA_Controller_AKIPPC_T));

  {
    int32_T i;
    static const char_T tmp[21] = { '/', 'e', 'l', 'a', 's', 't', 'i', 'c', '/',
      'j', 'o', 'i', 'n', 't', '_', 's', 't', 'a', 't', 'e', 's' };

    static const char_T tmp_0[15] = { 's', 'e', 'a', '_', 't', 'e', 't', 'a',
      'l', 'r', 'e', 'f', 'd', 'e', 'g' };

    static const char_T tmp_1[16] = { '/', 's', 'p', '/', 'j', 'o', 'i', 'n',
      't', '_', 's', 't', 'a', 't', 'e', 's' };

    char_T tmp_2[22];
    char_T tmp_3[16];
    char_T tmp_4[17];

    // Start for Constant: '<S6>/X0'
    for (i = 0; i < 6; i++) {
      SEA_Controller_AKIPPC_B.X0[i] = SEA_Controller_AKIPPC_P.X0_Value[i];
    }

    // End of Start for Constant: '<S6>/X0'

    // Start for Constant: '<S4>/Constant'
    SEA_Controller_AKIPPC_B.Constant = SEA_Controller_AKIPPC_P.Constant_Value_j;

    // Start for Atomic SubSystem: '<S3>/Publish'
    // Start for MATLABSystem: '<S59>/SinkBlock'
    SEA_Controller_AKIPPC_DW.obj_g.isInitialized = 0;
    SEA_Controller_AKIPPC_DW.obj_g.isInitialized = 1;
    for (i = 0; i < 16; i++) {
      tmp_4[i] = tmp_1[i];
    }

    tmp_4[16] = '\x00';
    Pub_SEA_Controller_AKIPPC_15.createPublisher(tmp_4,
      SEA_Controller__MessageQueueLen);

    // End of Start for MATLABSystem: '<S59>/SinkBlock'
    // End of Start for SubSystem: '<S3>/Publish'

    // Start for Atomic SubSystem: '<S2>/Subscribe1'
    // Start for MATLABSystem: '<S54>/SourceBlock'
    SEA_Controller_AKIPPC_DW.obj.isInitialized = 0;
    SEA_Controller_AKIPPC_DW.obj.isInitialized = 1;
    for (i = 0; i < 15; i++) {
      tmp_3[i] = tmp_0[i];
    }

    tmp_3[15] = '\x00';
    Sub_SEA_Controller_AKIPPC_35.createSubscriber(tmp_3,
      SEA_Controller__MessageQueueLen);

    // End of Start for MATLABSystem: '<S54>/SourceBlock'
    // End of Start for SubSystem: '<S2>/Subscribe1'

    // Start for Atomic SubSystem: '<S2>/Subscribe'
    // Start for MATLABSystem: '<S53>/SourceBlock'
    SEA_Controller_AKIPPC_DW.obj_p.isInitialized = 0;
    SEA_Controller_AKIPPC_DW.obj_p.isInitialized = 1;
    for (i = 0; i < 21; i++) {
      tmp_2[i] = tmp[i];
    }

    tmp_2[21] = '\x00';
    Sub_SEA_Controller_AKIPPC_14.createSubscriber(tmp_2,
      SEA_Controller__MessageQueueLen);

    // End of Start for MATLABSystem: '<S53>/SourceBlock'
    // End of Start for SubSystem: '<S2>/Subscribe'

    // InitializeConditions for Integrator: '<S6>/MemoryX' incorporates:
    //   InitializeConditions for Integrator: '<S4>/Integrator'

    if (rtmIsFirstInitCond(SEA_Controller_AKIPPC_M)) {
      SEA_Controller_AKIPPC_X.MemoryX_CSTATE[0] = 0.0;
      SEA_Controller_AKIPPC_X.MemoryX_CSTATE[1] = 0.0;
      SEA_Controller_AKIPPC_X.MemoryX_CSTATE[2] = 0.0;
      SEA_Controller_AKIPPC_X.MemoryX_CSTATE[3] = 0.0;
      SEA_Controller_AKIPPC_X.MemoryX_CSTATE[4] = 0.0;
      SEA_Controller_AKIPPC_X.MemoryX_CSTATE[5] = 0.0;
      SEA_Controller_AKIPPC_X.Integrator_CSTATE = 0.0;
    }

    SEA_Controller_AKIPPC_DW.MemoryX_IWORK = 1;

    // End of InitializeConditions for Integrator: '<S6>/MemoryX'

    // InitializeConditions for Integrator: '<S4>/Integrator'
    SEA_Controller_AKIPPC_DW.Integrator_IWORK = 1;

    // SystemInitialize for Enabled SubSystem: '<S2>/Reference sensing'
    // SystemInitialize for Outport: '<S52>/tetalrefdeg'
    SEA_Controller_AKIPPC_B.DataTypeConversion =
      SEA_Controller_AKIPPC_P.tetalrefdeg_Y0;

    // End of SystemInitialize for SubSystem: '<S2>/Reference sensing'

    // SystemInitialize for Enabled SubSystem: '<S2>/Joint sensing'
    // SystemInitialize for Outport: '<S51>/tetamrad'
    SEA_Controller_AKIPPC_B.pos_motor = SEA_Controller_AKIPPC_P.tetamrad_Y0;

    // SystemInitialize for Outport: '<S51>/tetalrad'
    SEA_Controller_AKIPPC_B.Sum1 = SEA_Controller_AKIPPC_P.tetalrad_Y0;

    // End of SystemInitialize for SubSystem: '<S2>/Joint sensing'

    // SystemInitialize for Enabled SubSystem: '<S25>/MeasurementUpdate'
    // SystemInitialize for Outport: '<S50>/L*(y[k]-yhat[k|k-1])'
    for (i = 0; i < 6; i++) {
      SEA_Controller_AKIPPC_B.Product3[i] =
        SEA_Controller_AKIPPC_P.Lykyhatkk1_Y0;
    }

    // End of SystemInitialize for Outport: '<S50>/L*(y[k]-yhat[k|k-1])'
    // End of SystemInitialize for SubSystem: '<S25>/MeasurementUpdate'
  }

  // set "at time zero" to false
  if (rtmIsFirstInitCond(SEA_Controller_AKIPPC_M)) {
    rtmSetFirstInitCond(SEA_Controller_AKIPPC_M, 0);
  }
}

// Model terminate function
void SEA_Controller_AKIPPC_terminate(void)
{
  // Terminate for Atomic SubSystem: '<S3>/Publish'
  // Start for MATLABSystem: '<S59>/SinkBlock' incorporates:
  //   Terminate for MATLABSystem: '<S59>/SinkBlock'

  if (SEA_Controller_AKIPPC_DW.obj_g.isInitialized == 1) {
    SEA_Controller_AKIPPC_DW.obj_g.isInitialized = 2;
  }

  // End of Start for MATLABSystem: '<S59>/SinkBlock'
  // End of Terminate for SubSystem: '<S3>/Publish'

  // Terminate for Atomic SubSystem: '<S2>/Subscribe1'
  // Start for MATLABSystem: '<S54>/SourceBlock' incorporates:
  //   Terminate for MATLABSystem: '<S54>/SourceBlock'

  if (SEA_Controller_AKIPPC_DW.obj.isInitialized == 1) {
    SEA_Controller_AKIPPC_DW.obj.isInitialized = 2;
  }

  // End of Start for MATLABSystem: '<S54>/SourceBlock'
  // End of Terminate for SubSystem: '<S2>/Subscribe1'

  // Terminate for Atomic SubSystem: '<S2>/Subscribe'
  // Start for MATLABSystem: '<S53>/SourceBlock' incorporates:
  //   Terminate for MATLABSystem: '<S53>/SourceBlock'

  if (SEA_Controller_AKIPPC_DW.obj_p.isInitialized == 1) {
    SEA_Controller_AKIPPC_DW.obj_p.isInitialized = 2;
  }

  // End of Start for MATLABSystem: '<S53>/SourceBlock'
  // End of Terminate for SubSystem: '<S2>/Subscribe'
}

//
// File trailer for generated code.
//
// [EOF]
//
