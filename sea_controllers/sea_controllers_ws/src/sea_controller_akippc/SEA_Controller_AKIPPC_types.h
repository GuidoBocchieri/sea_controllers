//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
//
// File: SEA_Controller_AKIPPC_types.h
//
// Code generated for Simulink model 'SEA_Controller_AKIPPC'.
//
// Model version                  : 1.15
// Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
// C/C++ source code generated on : Fri Sep  1 13:03:41 2017
//
// Target selection: ert.tlc
// Embedded hardware selection: Generic->Unspecified (assume 32-bit Generic)
// Code generation objectives: Unspecified
// Validation result: Not run
//
#ifndef RTW_HEADER_SEA_Controller_AKIPPC_types_h_
#define RTW_HEADER_SEA_Controller_AKIPPC_types_h_
#include "rtwtypes.h"
#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_ROSVariableLengthArrayInfo_
#define DEFINED_TYPEDEF_FOR_SL_Bus_ROSVariableLengthArrayInfo_

typedef struct {
  uint32_T CurrentLength;
  uint32_T ReceivedLength;
} SL_Bus_ROSVariableLengthArrayInfo;

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_SEA_Controller_AKIPPC_std_msgs_String_
#define DEFINED_TYPEDEF_FOR_SL_Bus_SEA_Controller_AKIPPC_std_msgs_String_

// MsgType=std_msgs/String
typedef struct {
  // PrimitiveROSType=string:IsVarLen=1:VarLenCategory=data:VarLenElem=Data_SL_Info:TruncateAction=warn 
  uint8_T Data[128];

  // IsVarLen=1:VarLenCategory=length:VarLenElem=Data
  SL_Bus_ROSVariableLengthArrayInfo Data_SL_Info;
} SL_Bus_SEA_Controller_AKIPPC_std_msgs_String;

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_SEA_Controller_AKIPPC_ros_time_Time_
#define DEFINED_TYPEDEF_FOR_SL_Bus_SEA_Controller_AKIPPC_ros_time_Time_

// MsgType=ros_time/Time
typedef struct {
  real_T Sec;
  real_T Nsec;
} SL_Bus_SEA_Controller_AKIPPC_ros_time_Time;

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_SEA_Controller_AKIPPC_std_msgs_Header_
#define DEFINED_TYPEDEF_FOR_SL_Bus_SEA_Controller_AKIPPC_std_msgs_Header_

// MsgType=std_msgs/Header
typedef struct {
  uint32_T Seq;

  // PrimitiveROSType=string:IsVarLen=1:VarLenCategory=data:VarLenElem=FrameId_SL_Info:TruncateAction=warn 
  uint8_T FrameId[128];

  // IsVarLen=1:VarLenCategory=length:VarLenElem=FrameId
  SL_Bus_ROSVariableLengthArrayInfo FrameId_SL_Info;

  // MsgType=ros_time/Time
  SL_Bus_SEA_Controller_AKIPPC_ros_time_Time Stamp;
} SL_Bus_SEA_Controller_AKIPPC_std_msgs_Header;

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_SEA_Controller_AKIPPC_sensor_msgs_JointState_
#define DEFINED_TYPEDEF_FOR_SL_Bus_SEA_Controller_AKIPPC_sensor_msgs_JointState_

// MsgType=sensor_msgs/JointState
typedef struct {
  // MsgType=std_msgs/String:PrimitiveROSType=string[]:IsVarLen=1:VarLenCategory=data:VarLenElem=Name_SL_Info:TruncateAction=warn 
  SL_Bus_SEA_Controller_AKIPPC_std_msgs_String Name[16];

  // IsVarLen=1:VarLenCategory=length:VarLenElem=Name
  SL_Bus_ROSVariableLengthArrayInfo Name_SL_Info;

  // IsVarLen=1:VarLenCategory=data:VarLenElem=Position_SL_Info:TruncateAction=warn 
  real_T Position[128];

  // IsVarLen=1:VarLenCategory=length:VarLenElem=Position
  SL_Bus_ROSVariableLengthArrayInfo Position_SL_Info;

  // IsVarLen=1:VarLenCategory=data:VarLenElem=Velocity_SL_Info:TruncateAction=warn 
  real_T Velocity[128];

  // IsVarLen=1:VarLenCategory=length:VarLenElem=Velocity
  SL_Bus_ROSVariableLengthArrayInfo Velocity_SL_Info;

  // IsVarLen=1:VarLenCategory=data:VarLenElem=Effort_SL_Info:TruncateAction=warn 
  real_T Effort[128];

  // IsVarLen=1:VarLenCategory=length:VarLenElem=Effort
  SL_Bus_ROSVariableLengthArrayInfo Effort_SL_Info;

  // MsgType=std_msgs/Header
  SL_Bus_SEA_Controller_AKIPPC_std_msgs_Header Header;
} SL_Bus_SEA_Controller_AKIPPC_sensor_msgs_JointState;

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_SEA_Controller_AKIPPC_std_msgs_Float32_
#define DEFINED_TYPEDEF_FOR_SL_Bus_SEA_Controller_AKIPPC_std_msgs_Float32_

// MsgType=std_msgs/Float32
typedef struct {
  real32_T Data;
} SL_Bus_SEA_Controller_AKIPPC_std_msgs_Float32;

#endif

#ifndef typedef_robotics_slros_internal_block_T
#define typedef_robotics_slros_internal_block_T

typedef struct {
  int32_T isInitialized;
} robotics_slros_internal_block_T;

#endif                                 //typedef_robotics_slros_internal_block_T

#ifndef typedef_robotics_slros_internal_blo_j_T
#define typedef_robotics_slros_internal_blo_j_T

typedef struct {
  int32_T isInitialized;
} robotics_slros_internal_blo_j_T;

#endif                                 //typedef_robotics_slros_internal_blo_j_T

#ifndef typedef_struct_T_SEA_Controller_AKIPP_T
#define typedef_struct_T_SEA_Controller_AKIPP_T

typedef struct {
  real_T f1[2];
} struct_T_SEA_Controller_AKIPP_T;

#endif                                 //typedef_struct_T_SEA_Controller_AKIPP_T

#ifndef typedef_struct_T_SEA_Controller_AKI_j_T
#define typedef_struct_T_SEA_Controller_AKI_j_T

typedef struct {
  char_T f1[4];
} struct_T_SEA_Controller_AKI_j_T;

#endif                                 //typedef_struct_T_SEA_Controller_AKI_j_T

#ifndef typedef_struct_T_SEA_Controller_AK_jj_T
#define typedef_struct_T_SEA_Controller_AK_jj_T

typedef struct {
  char_T f1[8];
} struct_T_SEA_Controller_AK_jj_T;

#endif                                 //typedef_struct_T_SEA_Controller_AK_jj_T

#ifndef typedef_struct_T_SEA_Controller_A_jjy_T
#define typedef_struct_T_SEA_Controller_A_jjy_T

typedef struct {
  char_T f1[7];
} struct_T_SEA_Controller_A_jjy_T;

#endif                                 //typedef_struct_T_SEA_Controller_A_jjy_T

#ifndef typedef_struct_T_SEA_Controller__jjyc_T
#define typedef_struct_T_SEA_Controller__jjyc_T

typedef struct {
  char_T f1[8];
  char_T f2[7];
  char_T f3[6];
} struct_T_SEA_Controller__jjyc_T;

#endif                                 //typedef_struct_T_SEA_Controller__jjyc_T

// Parameters (auto storage)
typedef struct P_SEA_Controller_AKIPPC_T_ P_SEA_Controller_AKIPPC_T;

// Forward declaration for rtModel
typedef struct tag_RTM_SEA_Controller_AKIPPC_T RT_MODEL_SEA_Controller_AKIPP_T;

#endif                                 // RTW_HEADER_SEA_Controller_AKIPPC_types_h_ 

//
// File trailer for generated code.
//
// [EOF]
//
