//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
//
// File: SEA_Controller_AKIPPC.h
//
// Code generated for Simulink model 'SEA_Controller_AKIPPC'.
//
// Model version                  : 1.15
// Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
// C/C++ source code generated on : Fri Sep  1 13:03:41 2017
//
// Target selection: ert.tlc
// Embedded hardware selection: Generic->Unspecified (assume 32-bit Generic)
// Code generation objectives: Unspecified
// Validation result: Not run
//
#ifndef RTW_HEADER_SEA_Controller_AKIPPC_h_
#define RTW_HEADER_SEA_Controller_AKIPPC_h_
#include <string.h>
#include <stddef.h>
#ifndef SEA_Controller_AKIPPC_COMMON_INCLUDES_
# define SEA_Controller_AKIPPC_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "slros_initialize.h"
#endif                                 // SEA_Controller_AKIPPC_COMMON_INCLUDES_ 

#include "SEA_Controller_AKIPPC_types.h"

// Macros for accessing real-time model data structure
#ifndef rtmGetBlkStateChangeFlag
# define rtmGetBlkStateChangeFlag(rtm) ((rtm)->blkStateChange)
#endif

#ifndef rtmSetBlkStateChangeFlag
# define rtmSetBlkStateChangeFlag(rtm, val) ((rtm)->blkStateChange = (val))
#endif

#ifndef rtmGetContStateDisabled
# define rtmGetContStateDisabled(rtm)  ((rtm)->contStateDisabled)
#endif

#ifndef rtmSetContStateDisabled
# define rtmSetContStateDisabled(rtm, val) ((rtm)->contStateDisabled = (val))
#endif

#ifndef rtmGetContStates
# define rtmGetContStates(rtm)         ((rtm)->contStates)
#endif

#ifndef rtmSetContStates
# define rtmSetContStates(rtm, val)    ((rtm)->contStates = (val))
#endif

#ifndef rtmGetDerivCacheNeedsReset
# define rtmGetDerivCacheNeedsReset(rtm) ((rtm)->derivCacheNeedsReset)
#endif

#ifndef rtmSetDerivCacheNeedsReset
# define rtmSetDerivCacheNeedsReset(rtm, val) ((rtm)->derivCacheNeedsReset = (val))
#endif

#ifndef rtmGetIntgData
# define rtmGetIntgData(rtm)           ((rtm)->intgData)
#endif

#ifndef rtmSetIntgData
# define rtmSetIntgData(rtm, val)      ((rtm)->intgData = (val))
#endif

#ifndef rtmGetOdeF
# define rtmGetOdeF(rtm)               ((rtm)->odeF)
#endif

#ifndef rtmSetOdeF
# define rtmSetOdeF(rtm, val)          ((rtm)->odeF = (val))
#endif

#ifndef rtmGetOdeY
# define rtmGetOdeY(rtm)               ((rtm)->odeY)
#endif

#ifndef rtmSetOdeY
# define rtmSetOdeY(rtm, val)          ((rtm)->odeY = (val))
#endif

#ifndef rtmGetPeriodicContStateIndices
# define rtmGetPeriodicContStateIndices(rtm) ((rtm)->periodicContStateIndices)
#endif

#ifndef rtmSetPeriodicContStateIndices
# define rtmSetPeriodicContStateIndices(rtm, val) ((rtm)->periodicContStateIndices = (val))
#endif

#ifndef rtmGetPeriodicContStateRanges
# define rtmGetPeriodicContStateRanges(rtm) ((rtm)->periodicContStateRanges)
#endif

#ifndef rtmSetPeriodicContStateRanges
# define rtmSetPeriodicContStateRanges(rtm, val) ((rtm)->periodicContStateRanges = (val))
#endif

#ifndef rtmGetZCCacheNeedsReset
# define rtmGetZCCacheNeedsReset(rtm)  ((rtm)->zCCacheNeedsReset)
#endif

#ifndef rtmSetZCCacheNeedsReset
# define rtmSetZCCacheNeedsReset(rtm, val) ((rtm)->zCCacheNeedsReset = (val))
#endif

#ifndef rtmGetdX
# define rtmGetdX(rtm)                 ((rtm)->derivs)
#endif

#ifndef rtmSetdX
# define rtmSetdX(rtm, val)            ((rtm)->derivs = (val))
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  (rtmGetTPtr((rtm))[0])
#endif

// Block signals (auto storage)
typedef struct {
  SL_Bus_SEA_Controller_AKIPPC_sensor_msgs_JointState msg;// '<S3>/Assign'
  real_T X0[6];                        // '<S6>/X0'
  real_T Add[6];                       // '<S25>/Add'
  real_T dv0[6];
  real_T dv1[5];
  real_T dv2[2];
  real_T Constant;                     // '<S4>/Constant'
  real_T error;                        // '<S4>/Sum'
  real_T DataTypeConversion;           // '<S52>/Data Type Conversion'
  real_T pos_motor;                    // '<S51>/Selector1'
  real_T Sum1;                         // '<S51>/Sum1'
  real_T Product3[6];                  // '<S50>/Product3'
} B_SEA_Controller_AKIPPC_T;

// Block states (auto storage) for system '<Root>'
typedef struct {
  void *SinkBlock_PWORK;               // '<S59>/SinkBlock'
  void *SourceBlock_PWORK;             // '<S54>/SourceBlock'
  void *SourceBlock_PWORK_e;           // '<S53>/SourceBlock'
  robotics_slros_internal_block_T obj; // '<S54>/SourceBlock'
  robotics_slros_internal_block_T obj_p;// '<S53>/SourceBlock'
  robotics_slros_internal_blo_j_T obj_g;// '<S59>/SinkBlock'
  int_T MemoryX_IWORK;                 // '<S6>/MemoryX'
  int_T Integrator_IWORK;              // '<S4>/Integrator'
  boolean_T MeasurementUpdate_MODE;    // '<S25>/MeasurementUpdate'
} DW_SEA_Controller_AKIPPC_T;

// Continuous states (auto storage)
typedef struct {
  real_T MemoryX_CSTATE[6];            // '<S6>/MemoryX'
  real_T Integrator_CSTATE;            // '<S4>/Integrator'
} X_SEA_Controller_AKIPPC_T;

// State derivatives (auto storage)
typedef struct {
  real_T MemoryX_CSTATE[6];            // '<S6>/MemoryX'
  real_T Integrator_CSTATE;            // '<S4>/Integrator'
} XDot_SEA_Controller_AKIPPC_T;

// State disabled
typedef struct {
  boolean_T MemoryX_CSTATE[6];         // '<S6>/MemoryX'
  boolean_T Integrator_CSTATE;         // '<S4>/Integrator'
} XDis_SEA_Controller_AKIPPC_T;

#ifndef ODE3_INTG
#define ODE3_INTG

// ODE3 Integration Data
typedef struct {
  real_T *y;                           // output
  real_T *f[3];                        // derivatives
} ODE3_IntgData;

#endif

// Parameters (auto storage)
struct P_SEA_Controller_AKIPPC_T_ {
  real_T gain[5];                      // Variable: gain
                                       //  Referenced by: '<S4>/Gain'

  SL_Bus_SEA_Controller_AKIPPC_sensor_msgs_JointState Out1_Y0;// Computed Parameter: Out1_Y0
                                                              //  Referenced by: '<S55>/Out1'

  SL_Bus_SEA_Controller_AKIPPC_sensor_msgs_JointState Constant_Value;// Computed Parameter: Constant_Value
                                                                     //  Referenced by: '<S53>/Constant'

  SL_Bus_SEA_Controller_AKIPPC_sensor_msgs_JointState Constant_Value_h;// Computed Parameter: Constant_Value_h
                                                                      //  Referenced by: '<S58>/Constant'

  real_T Lykyhatkk1_Y0;                // Expression: 0
                                       //  Referenced by: '<S50>/L*(y[k]-yhat[k|k-1])'

  real_T tetamrad_Y0;                  // Computed Parameter: tetamrad_Y0
                                       //  Referenced by: '<S51>/tetamrad'

  real_T tetalrad_Y0;                  // Computed Parameter: tetalrad_Y0
                                       //  Referenced by: '<S51>/tetalrad'

  real_T Constant2_Value;              // Expression: 1
                                       //  Referenced by: '<S51>/Constant2'

  real_T Constant4_Value;              // Expression: 2
                                       //  Referenced by: '<S51>/Constant4'

  real_T tetalrefdeg_Y0;               // Computed Parameter: tetalrefdeg_Y0
                                       //  Referenced by: '<S52>/tetalrefdeg'

  real_T X0_Value[6];                  // Expression: pInitialization.X0
                                       //  Referenced by: '<S6>/X0'

  real_T Constant_Value_j;             // Expression: 0
                                       //  Referenced by: '<S4>/Constant'

  real_T Gain1_Gain;                   // Expression: pi/180
                                       //  Referenced by: '<S5>/Gain1'

  real_T A_Value[36];                  // Expression: pInitialization.A
                                       //  Referenced by: '<S6>/A'

  real_T B_Value[6];                   // Expression: pInitialization.B
                                       //  Referenced by: '<S6>/B'

  real_T C_Value[12];                  // Expression: pInitialization.C
                                       //  Referenced by: '<S6>/C'

  real_T KalmanGainL_Value[12];        // Expression: pInitialization.L
                                       //  Referenced by: '<S7>/KalmanGainL'

  real_T D_Value[2];                   // Expression: pInitialization.D
                                       //  Referenced by: '<S6>/D'

  SL_Bus_SEA_Controller_AKIPPC_std_msgs_Float32 Out1_Y0_i;// Computed Parameter: Out1_Y0_i
                                                          //  Referenced by: '<S56>/Out1'

  SL_Bus_SEA_Controller_AKIPPC_std_msgs_Float32 Constant_Value_b;// Computed Parameter: Constant_Value_b
                                                                 //  Referenced by: '<S54>/Constant'

  boolean_T Enable_Value;              // Computed Parameter: Enable_Value
                                       //  Referenced by: '<S6>/Enable'

};

// Real-time Model Data Structure
struct tag_RTM_SEA_Controller_AKIPPC_T {
  const char_T *errorStatus;
  RTWSolverInfo solverInfo;
  X_SEA_Controller_AKIPPC_T *contStates;
  int_T *periodicContStateIndices;
  real_T *periodicContStateRanges;
  real_T *derivs;
  boolean_T *contStateDisabled;
  boolean_T zCCacheNeedsReset;
  boolean_T derivCacheNeedsReset;
  boolean_T blkStateChange;
  real_T odeY[7];
  real_T odeF[3][7];
  ODE3_IntgData intgData;

  //
  //  Sizes:
  //  The following substructure contains sizes information
  //  for many of the model attributes such as inputs, outputs,
  //  dwork, sample times, etc.

  struct {
    int_T numContStates;
    int_T numPeriodicContStates;
    int_T numSampTimes;
  } Sizes;

  //
  //  Timing:
  //  The following substructure contains information regarding
  //  the timing information for the model.

  struct {
    uint32_T clockTick0;
    time_T stepSize0;
    uint32_T clockTick1;
    boolean_T firstInitCondFlag;
    SimTimeStep simTimeStep;
    boolean_T stopRequestedFlag;
    time_T *t;
    time_T tArray[2];
  } Timing;
};

// Block parameters (auto storage)
#ifdef __cplusplus

extern "C" {

#endif

  extern P_SEA_Controller_AKIPPC_T SEA_Controller_AKIPPC_P;

#ifdef __cplusplus

}
#endif

// Block signals (auto storage)
extern B_SEA_Controller_AKIPPC_T SEA_Controller_AKIPPC_B;

// Continuous states (auto storage)
extern X_SEA_Controller_AKIPPC_T SEA_Controller_AKIPPC_X;

// Block states (auto storage)
extern DW_SEA_Controller_AKIPPC_T SEA_Controller_AKIPPC_DW;

#ifdef __cplusplus

extern "C" {

#endif

#ifdef __cplusplus

}
#endif

#ifdef __cplusplus

extern "C" {

#endif

  // Model entry point functions
  extern void SEA_Controller_AKIPPC_initialize(void);
  extern void SEA_Controller_AKIPPC_step(void);
  extern void SEA_Controller_AKIPPC_terminate(void);

#ifdef __cplusplus

}
#endif

// Real-time Model object
#ifdef __cplusplus

extern "C" {

#endif

  extern RT_MODEL_SEA_Controller_AKIPP_T *const SEA_Controller_AKIPPC_M;

#ifdef __cplusplus

}
#endif

//-
//  These blocks were eliminated from the model due to optimizations:
//
//  Block '<S7>/ConstantP' : Unused code path elimination
//  Block '<S7>/CovarianceZ' : Unused code path elimination
//  Block '<S46>/Data Type Duplicate' : Unused code path elimination
//  Block '<S47>/Conversion' : Unused code path elimination
//  Block '<S47>/Data Type Duplicate' : Unused code path elimination
//  Block '<S48>/Conversion' : Unused code path elimination
//  Block '<S48>/Data Type Duplicate' : Unused code path elimination
//  Block '<S49>/Conversion' : Unused code path elimination
//  Block '<S49>/Data Type Duplicate' : Unused code path elimination
//  Block '<S7>/KalmanGainM' : Unused code path elimination
//  Block '<S8>/Add1' : Unused code path elimination
//  Block '<S8>/Product' : Unused code path elimination
//  Block '<S8>/Product1' : Unused code path elimination
//  Block '<S9>/Data Type Duplicate' : Unused code path elimination
//  Block '<S10>/Data Type Duplicate' : Unused code path elimination
//  Block '<S11>/Data Type Duplicate' : Unused code path elimination
//  Block '<S12>/Data Type Duplicate' : Unused code path elimination
//  Block '<S13>/Conversion' : Unused code path elimination
//  Block '<S13>/Data Type Duplicate' : Unused code path elimination
//  Block '<S14>/Conversion' : Unused code path elimination
//  Block '<S14>/Data Type Duplicate' : Unused code path elimination
//  Block '<S15>/Conversion' : Unused code path elimination
//  Block '<S15>/Data Type Duplicate' : Unused code path elimination
//  Block '<S16>/Conversion' : Unused code path elimination
//  Block '<S16>/Data Type Duplicate' : Unused code path elimination
//  Block '<S17>/Conversion' : Unused code path elimination
//  Block '<S17>/Data Type Duplicate' : Unused code path elimination
//  Block '<S18>/Conversion' : Unused code path elimination
//  Block '<S18>/Data Type Duplicate' : Unused code path elimination
//  Block '<S19>/Conversion' : Unused code path elimination
//  Block '<S19>/Data Type Duplicate' : Unused code path elimination
//  Block '<S20>/Conversion' : Unused code path elimination
//  Block '<S20>/Data Type Duplicate' : Unused code path elimination
//  Block '<S21>/Data Type Duplicate' : Unused code path elimination
//  Block '<S22>/Data Type Duplicate' : Unused code path elimination
//  Block '<S6>/G' : Unused code path elimination
//  Block '<S6>/H' : Unused code path elimination
//  Block '<S6>/ManualSwitchPZ' : Unused code path elimination
//  Block '<S6>/N' : Unused code path elimination
//  Block '<S6>/P0' : Unused code path elimination
//  Block '<S6>/Q' : Unused code path elimination
//  Block '<S6>/R' : Unused code path elimination
//  Block '<S26>/Constant' : Unused code path elimination
//  Block '<S6>/Reset' : Unused code path elimination
//  Block '<S6>/Reshapeyhat' : Unused code path elimination
//  Block '<S44>/CheckSignalProperties' : Unused code path elimination
//  Block '<S45>/CheckSignalProperties' : Unused code path elimination
//  Block '<S4>/P' : Unused code path elimination
//  Block '<S4>/Sum2' : Unused code path elimination
//  Block '<S4>/tau friction' : Unused code path elimination
//  Block '<S4>/tau l' : Unused code path elimination
//  Block '<S4>/ydiff' : Unused code path elimination
//  Block '<S46>/Conversion' : Eliminate redundant data type conversion
//  Block '<S9>/Conversion' : Eliminate redundant data type conversion
//  Block '<S10>/Conversion' : Eliminate redundant data type conversion
//  Block '<S11>/Conversion' : Eliminate redundant data type conversion
//  Block '<S12>/Conversion' : Eliminate redundant data type conversion
//  Block '<S6>/DataTypeConversionEnable' : Eliminate redundant data type conversion
//  Block '<S21>/Conversion' : Eliminate redundant data type conversion
//  Block '<S22>/Conversion' : Eliminate redundant data type conversion
//  Block '<S25>/Reshape' : Reshape block reduction
//  Block '<S6>/ReshapeX0' : Reshape block reduction
//  Block '<S6>/Reshapeu' : Reshape block reduction
//  Block '<S6>/Reshapexhat' : Reshape block reduction
//  Block '<S6>/Reshapey' : Reshape block reduction
//  Block '<S51>/Data Type Conversion' : Eliminate redundant data type conversion
//  Block '<S51>/Data Type Conversion1' : Eliminate redundant data type conversion


//-
//  The generated code includes comments that allow you to trace directly
//  back to the appropriate location in the model.  The basic format
//  is <system>/block_name, where system is the system number (uniquely
//  assigned by Simulink) and block_name is the name of the block.
//
//  Use the MATLAB hilite_system command to trace the generated code back
//  to the model.  For example,
//
//  hilite_system('<S3>')    - opens system 3
//  hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
//
//  Here is the system hierarchy for this model
//
//  '<Root>' : 'SEA_Controller_AKIPPC'
//  '<S1>'   : 'SEA_Controller_AKIPPC/Controller'
//  '<S2>'   : 'SEA_Controller_AKIPPC/Input Interface'
//  '<S3>'   : 'SEA_Controller_AKIPPC/Output Interface'
//  '<S4>'   : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral'
//  '<S5>'   : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Degrees to Radians'
//  '<S6>'   : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter'
//  '<S7>'   : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/CalculatePL'
//  '<S8>'   : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/CalculateYhat'
//  '<S9>'   : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/DataTypeConversionA'
//  '<S10>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/DataTypeConversionB'
//  '<S11>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/DataTypeConversionC'
//  '<S12>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/DataTypeConversionD'
//  '<S13>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/DataTypeConversionG'
//  '<S14>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/DataTypeConversionH'
//  '<S15>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/DataTypeConversionN'
//  '<S16>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/DataTypeConversionP'
//  '<S17>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/DataTypeConversionP0'
//  '<S18>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/DataTypeConversionQ'
//  '<S19>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/DataTypeConversionR'
//  '<S20>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/DataTypeConversionReset'
//  '<S21>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/DataTypeConversionX'
//  '<S22>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/DataTypeConversionX0'
//  '<S23>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/DataTypeConversionu'
//  '<S24>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/MemoryP'
//  '<S25>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/Observer'
//  '<S26>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/ReducedQRN'
//  '<S27>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/ScalarExpansionP0'
//  '<S28>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/ScalarExpansionQ'
//  '<S29>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/ScalarExpansionR'
//  '<S30>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/UseCurrentEstimator'
//  '<S31>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/checkA'
//  '<S32>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/checkB'
//  '<S33>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/checkC'
//  '<S34>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/checkD'
//  '<S35>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/checkEnable'
//  '<S36>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/checkG'
//  '<S37>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/checkH'
//  '<S38>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/checkN'
//  '<S39>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/checkP0'
//  '<S40>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/checkQ'
//  '<S41>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/checkR'
//  '<S42>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/checkReset'
//  '<S43>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/checkX0'
//  '<S44>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/checku'
//  '<S45>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/checky'
//  '<S46>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/CalculatePL/DataTypeConversionL'
//  '<S47>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/CalculatePL/DataTypeConversionM'
//  '<S48>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/CalculatePL/DataTypeConversionP'
//  '<S49>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/CalculatePL/DataTypeConversionZ'
//  '<S50>'  : 'SEA_Controller_AKIPPC/Controller/Augmented  Kalman + Pole Placement + Integral/Kalman Filter/Observer/MeasurementUpdate'
//  '<S51>'  : 'SEA_Controller_AKIPPC/Input Interface/Joint sensing'
//  '<S52>'  : 'SEA_Controller_AKIPPC/Input Interface/Reference sensing'
//  '<S53>'  : 'SEA_Controller_AKIPPC/Input Interface/Subscribe'
//  '<S54>'  : 'SEA_Controller_AKIPPC/Input Interface/Subscribe1'
//  '<S55>'  : 'SEA_Controller_AKIPPC/Input Interface/Subscribe/Enabled Subsystem'
//  '<S56>'  : 'SEA_Controller_AKIPPC/Input Interface/Subscribe1/Enabled Subsystem'
//  '<S57>'  : 'SEA_Controller_AKIPPC/Output Interface/Assign'
//  '<S58>'  : 'SEA_Controller_AKIPPC/Output Interface/Blank Message'
//  '<S59>'  : 'SEA_Controller_AKIPPC/Output Interface/Publish'

#endif                                 // RTW_HEADER_SEA_Controller_AKIPPC_h_

//
// File trailer for generated code.
//
// [EOF]
//
