#ifndef _SLROS_INITIALIZE_H_
#define _SLROS_INITIALIZE_H_

#include "slros_busmsg_conversion.h"
#include "slros_generic.h"

extern ros::NodeHandle * SLROSNodePtr;
extern const std::string SLROSNodeName;

// For Block SEA_Controller_KIPPC/Input Interface/Subscribe
extern SimulinkSubscriber<sensor_msgs::JointState, SL_Bus_SEA_Controller_KIPPC_sensor_msgs_JointState> Sub_SEA_Controller_KIPPC_14;

// For Block SEA_Controller_KIPPC/Input Interface/Subscribe1
extern SimulinkSubscriber<std_msgs::Float32, SL_Bus_SEA_Controller_KIPPC_std_msgs_Float32> Sub_SEA_Controller_KIPPC_35;

// For Block SEA_Controller_KIPPC/Output Interface/Publish
extern SimulinkPublisher<sensor_msgs::JointState, SL_Bus_SEA_Controller_KIPPC_sensor_msgs_JointState> Pub_SEA_Controller_KIPPC_15;

void slros_node_init(int argc, char** argv);

#endif
