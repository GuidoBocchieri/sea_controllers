//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
//
// File: SEA_Controller_KIPPC.cpp
//
// Code generated for Simulink model 'SEA_Controller_KIPPC'.
//
// Model version                  : 1.12
// Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
// C/C++ source code generated on : Tue Jul 11 13:57:24 2017
//
// Target selection: ert.tlc
// Embedded hardware selection: Generic->Unspecified (assume 32-bit Generic)
// Code generation objectives: Unspecified
// Validation result: Not run
//
#include "SEA_Controller_KIPPC.h"
#include "SEA_Controller_KIPPC_private.h"
#define SEA_Controller__MessageQueueLen (1)

// Block signals (auto storage)
B_SEA_Controller_KIPPC_T SEA_Controller_KIPPC_B;

// Block states (auto storage)
DW_SEA_Controller_KIPPC_T SEA_Controller_KIPPC_DW;

// Real-time model
RT_MODEL_SEA_Controller_KIPPC_T SEA_Controller_KIPPC_M_;
RT_MODEL_SEA_Controller_KIPPC_T *const SEA_Controller_KIPPC_M =
  &SEA_Controller_KIPPC_M_;

// Model step function
void SEA_Controller_KIPPC_step(void)
{
  SL_Bus_SEA_Controller_KIPPC_std_msgs_Float32 varargout_2;
  boolean_T varargout_1;
  real_T rtb_taum_0;
  int32_T i;
  real_T tmp[2];
  real_T tmp_0[2];
  real_T tmp_1;

  // Delay: '<S6>/MemoryX' incorporates:
  //   Constant: '<S6>/X0'

  if (SEA_Controller_KIPPC_DW.icLoad != 0) {
    SEA_Controller_KIPPC_DW.MemoryX_DSTATE[0] = SEA_Controller_KIPPC_P.X0_Value
      [0];
    SEA_Controller_KIPPC_DW.MemoryX_DSTATE[1] = SEA_Controller_KIPPC_P.X0_Value
      [1];
    SEA_Controller_KIPPC_DW.MemoryX_DSTATE[2] = SEA_Controller_KIPPC_P.X0_Value
      [2];
    SEA_Controller_KIPPC_DW.MemoryX_DSTATE[3] = SEA_Controller_KIPPC_P.X0_Value
      [3];
  }

  // SignalConversion: '<S4>/TmpSignal ConversionAtGainInport1' incorporates:
  //   Delay: '<S6>/MemoryX'
  //   DiscreteIntegrator: '<S4>/Discrete-Time Integrator'

  SEA_Controller_KIPPC_B.dv0[0] = SEA_Controller_KIPPC_DW.MemoryX_DSTATE[0];
  SEA_Controller_KIPPC_B.dv0[1] = SEA_Controller_KIPPC_DW.MemoryX_DSTATE[1];
  SEA_Controller_KIPPC_B.dv0[2] = SEA_Controller_KIPPC_DW.MemoryX_DSTATE[2];
  SEA_Controller_KIPPC_B.dv0[3] = SEA_Controller_KIPPC_DW.MemoryX_DSTATE[3];
  SEA_Controller_KIPPC_B.dv0[4] =
    SEA_Controller_KIPPC_DW.DiscreteTimeIntegrator_DSTATE;

  // Gain: '<S4>/Gain' incorporates:
  //   SignalConversion: '<S4>/TmpSignal ConversionAtGainInport1'

  rtb_taum_0 = 0.0;
  for (i = 0; i < 5; i++) {
    rtb_taum_0 += SEA_Controller_KIPPC_P.gainDiscrete[i] *
      SEA_Controller_KIPPC_B.dv0[i];
  }

  // MATLAB Function: '<S3>/Assign' incorporates:
  //   Constant: '<S58>/Constant'
  //   Gain: '<S4>/Gain'

  SEA_Controller_KIPPC_B.msg = SEA_Controller_KIPPC_P.Constant_Value_h;

  //  To set datatype of MSG output:
  //   1. Create buses for this model: robotics.ros.createSimulinkBus(gcs)
  //   1. Click "Edit Data" in  Toolstrip to open "Ports and Data Manager"
  //   2. Select MSG and set datatype to "Bus: SL_Bus_<modelname>_<messageType>" 
  //  See:
  //  http://www.mathworks.com/help/simulink/ug/create-structures-in-matlab-function-blocks.html 
  // MATLAB Function 'Output Interface/Assign': '<S57>:1'
  //  blankMessage (sensor_msgs/LaserScan) consists of
  //              Header: [1x1 Header]
  //            AngleMin: 0
  //            AngleMax: 0
  //      AngleIncrement: 0
  //       TimeIncrement: 0
  //            ScanTime: 0
  //            RangeMin: 0
  //            RangeMax: 0
  //              Ranges: [0x1 single]
  //         Intensities: [0x1 single]
  //  Set Ranges array to have length 10, and modify indices 1, 3, 5 and 7
  // '<S57>:1:24' blankMessage.Effort_SL_Info.CurrentLength = uint32(1);
  SEA_Controller_KIPPC_B.msg.Effort_SL_Info.CurrentLength = 1U;

  // '<S57>:1:25' blankMessage.Effort([1]) = u;
  SEA_Controller_KIPPC_B.msg.Effort[0] = rtb_taum_0;

  // Outputs for Atomic SubSystem: '<S3>/Publish'
  // Start for MATLABSystem: '<S59>/SinkBlock' incorporates:
  //   MATLABSystem: '<S59>/SinkBlock'

  // '<S57>:1:27' msg = blankMessage;
  Pub_SEA_Controller_KIPPC_15.publish(&SEA_Controller_KIPPC_B.msg);

  // End of Outputs for SubSystem: '<S3>/Publish'

  // Outputs for Atomic SubSystem: '<S2>/Subscribe1'
  // Start for MATLABSystem: '<S54>/SourceBlock' incorporates:
  //   MATLABSystem: '<S54>/SourceBlock'

  varargout_1 = Sub_SEA_Controller_KIPPC_35.getLatestMessage(&varargout_2);

  // Outputs for Enabled SubSystem: '<S2>/Reference sensing' incorporates:
  //   EnablePort: '<S52>/Enable'

  // Outputs for Enabled SubSystem: '<S54>/Enabled Subsystem' incorporates:
  //   EnablePort: '<S56>/Enable'

  if (varargout_1) {
    // DataTypeConversion: '<S52>/Data Type Conversion'
    SEA_Controller_KIPPC_B.DataTypeConversion = varargout_2.Data;
  }

  // End of Start for MATLABSystem: '<S54>/SourceBlock'
  // End of Outputs for SubSystem: '<S54>/Enabled Subsystem'
  // End of Outputs for SubSystem: '<S2>/Reference sensing'
  // End of Outputs for SubSystem: '<S2>/Subscribe1'

  // Outputs for Atomic SubSystem: '<S2>/Subscribe'
  // Start for MATLABSystem: '<S53>/SourceBlock' incorporates:
  //   MATLABSystem: '<S53>/SourceBlock'

  varargout_1 = Sub_SEA_Controller_KIPPC_14.getLatestMessage
    (&SEA_Controller_KIPPC_B.msg);

  // Outputs for Enabled SubSystem: '<S2>/Joint sensing' incorporates:
  //   EnablePort: '<S51>/Enable'

  // Outputs for Enabled SubSystem: '<S53>/Enabled Subsystem' incorporates:
  //   EnablePort: '<S55>/Enable'

  if (varargout_1) {
    // Selector: '<S51>/Selector1' incorporates:
    //   Constant: '<S51>/Constant2'

    SEA_Controller_KIPPC_B.pos_motor = SEA_Controller_KIPPC_B.msg.Position
      [(int32_T)SEA_Controller_KIPPC_P.Constant2_Value - 1];

    // Sum: '<S51>/Sum1' incorporates:
    //   Constant: '<S51>/Constant4'
    //   Selector: '<S51>/Selector3'

    SEA_Controller_KIPPC_B.Sum1 = SEA_Controller_KIPPC_B.msg.Position[(int32_T)
      SEA_Controller_KIPPC_P.Constant4_Value - 1] +
      SEA_Controller_KIPPC_B.pos_motor;
  }

  // End of Start for MATLABSystem: '<S53>/SourceBlock'
  // End of Outputs for SubSystem: '<S53>/Enabled Subsystem'
  // End of Outputs for SubSystem: '<S2>/Joint sensing'
  // End of Outputs for SubSystem: '<S2>/Subscribe'

  // Outputs for Enabled SubSystem: '<S25>/MeasurementUpdate' incorporates:
  //   EnablePort: '<S50>/Enable'

  // Constant: '<S6>/Enable'
  if (SEA_Controller_KIPPC_P.Enable_Value) {
    if (!SEA_Controller_KIPPC_DW.MeasurementUpdate_MODE) {
      SEA_Controller_KIPPC_DW.MeasurementUpdate_MODE = true;
    }

    // Sum: '<S50>/Sum'
    tmp[0] = SEA_Controller_KIPPC_B.pos_motor;
    tmp[1] = SEA_Controller_KIPPC_B.Sum1;
    for (i = 0; i < 2; i++) {
      // Product: '<S50>/C[k]*xhat[k|k-1]' incorporates:
      //   Constant: '<S6>/C'
      //   Delay: '<S6>/MemoryX'
      //   Sum: '<S50>/Add1'

      tmp_1 = SEA_Controller_KIPPC_P.C_Value[i + 6] *
        SEA_Controller_KIPPC_DW.MemoryX_DSTATE[3] +
        (SEA_Controller_KIPPC_P.C_Value[i + 4] *
         SEA_Controller_KIPPC_DW.MemoryX_DSTATE[2] +
         (SEA_Controller_KIPPC_P.C_Value[i + 2] *
          SEA_Controller_KIPPC_DW.MemoryX_DSTATE[1] +
          SEA_Controller_KIPPC_P.C_Value[i] *
          SEA_Controller_KIPPC_DW.MemoryX_DSTATE[0]));

      // Sum: '<S50>/Sum' incorporates:
      //   Constant: '<S6>/D'
      //   Gain: '<S4>/Gain'
      //   Product: '<S50>/D[k]*u[k]'
      //   Product: '<S50>/Product3'
      //   Sum: '<S50>/Add1'

      tmp_0[i] = tmp[i] - (SEA_Controller_KIPPC_P.D_Value[i] * rtb_taum_0 +
                           tmp_1);
    }

    // Product: '<S50>/Product3' incorporates:
    //   Constant: '<S7>/KalmanGainL'

    for (i = 0; i < 4; i++) {
      SEA_Controller_KIPPC_B.Product3[i] = 0.0;
      SEA_Controller_KIPPC_B.Product3[i] +=
        SEA_Controller_KIPPC_P.KalmanGainL_Value[i] * tmp_0[0];
      SEA_Controller_KIPPC_B.Product3[i] +=
        SEA_Controller_KIPPC_P.KalmanGainL_Value[i + 4] * tmp_0[1];
    }
  } else {
    if (SEA_Controller_KIPPC_DW.MeasurementUpdate_MODE) {
      // Disable for Outport: '<S50>/L*(y[k]-yhat[k|k-1])'
      SEA_Controller_KIPPC_B.Product3[0] = SEA_Controller_KIPPC_P.Lykyhatkk1_Y0;
      SEA_Controller_KIPPC_B.Product3[1] = SEA_Controller_KIPPC_P.Lykyhatkk1_Y0;
      SEA_Controller_KIPPC_B.Product3[2] = SEA_Controller_KIPPC_P.Lykyhatkk1_Y0;
      SEA_Controller_KIPPC_B.Product3[3] = SEA_Controller_KIPPC_P.Lykyhatkk1_Y0;
      SEA_Controller_KIPPC_DW.MeasurementUpdate_MODE = false;
    }
  }

  // End of Constant: '<S6>/Enable'
  // End of Outputs for SubSystem: '<S25>/MeasurementUpdate'

  // Update for Delay: '<S6>/MemoryX'
  SEA_Controller_KIPPC_DW.icLoad = 0U;

  // Product: '<S25>/A[k]*xhat[k|k-1]' incorporates:
  //   Constant: '<S6>/A'
  //   Delay: '<S6>/MemoryX'
  //   Sum: '<S25>/Add'

  for (i = 0; i < 4; i++) {
    tmp_1 = SEA_Controller_KIPPC_P.A_Value[i + 12] *
      SEA_Controller_KIPPC_DW.MemoryX_DSTATE[3] +
      (SEA_Controller_KIPPC_P.A_Value[i + 8] *
       SEA_Controller_KIPPC_DW.MemoryX_DSTATE[2] +
       (SEA_Controller_KIPPC_P.A_Value[i + 4] *
        SEA_Controller_KIPPC_DW.MemoryX_DSTATE[1] +
        SEA_Controller_KIPPC_P.A_Value[i] *
        SEA_Controller_KIPPC_DW.MemoryX_DSTATE[0]));
    SEA_Controller_KIPPC_B.dv1[i] = tmp_1;
  }

  // End of Product: '<S25>/A[k]*xhat[k|k-1]'

  // Update for Delay: '<S6>/MemoryX' incorporates:
  //   Constant: '<S6>/B'
  //   Gain: '<S4>/Gain'
  //   Product: '<S25>/B[k]*u[k]'
  //   Sum: '<S25>/Add'

  SEA_Controller_KIPPC_DW.MemoryX_DSTATE[0] = (SEA_Controller_KIPPC_P.B_Value[0]
    * rtb_taum_0 + SEA_Controller_KIPPC_B.dv1[0]) +
    SEA_Controller_KIPPC_B.Product3[0];
  SEA_Controller_KIPPC_DW.MemoryX_DSTATE[1] = (SEA_Controller_KIPPC_P.B_Value[1]
    * rtb_taum_0 + SEA_Controller_KIPPC_B.dv1[1]) +
    SEA_Controller_KIPPC_B.Product3[1];
  SEA_Controller_KIPPC_DW.MemoryX_DSTATE[2] = (SEA_Controller_KIPPC_P.B_Value[2]
    * rtb_taum_0 + SEA_Controller_KIPPC_B.dv1[2]) +
    SEA_Controller_KIPPC_B.Product3[2];
  SEA_Controller_KIPPC_DW.MemoryX_DSTATE[3] = (SEA_Controller_KIPPC_P.B_Value[3]
    * rtb_taum_0 + SEA_Controller_KIPPC_B.dv1[3]) +
    SEA_Controller_KIPPC_B.Product3[3];

  // Update for DiscreteIntegrator: '<S4>/Discrete-Time Integrator' incorporates:
  //   Gain: '<S5>/Gain1'
  //   Sum: '<S4>/Sum'

  SEA_Controller_KIPPC_DW.DiscreteTimeIntegrator_DSTATE +=
    (SEA_Controller_KIPPC_B.Sum1 - SEA_Controller_KIPPC_P.Gain1_Gain *
     SEA_Controller_KIPPC_B.DataTypeConversion) *
    SEA_Controller_KIPPC_P.DiscreteTimeIntegrator_gainval;
}

// Model initialize function
void SEA_Controller_KIPPC_initialize(void)
{
  // Registration code

  // initialize error status
  rtmSetErrorStatus(SEA_Controller_KIPPC_M, (NULL));

  // block I/O
  (void) memset(((void *) &SEA_Controller_KIPPC_B), 0,
                sizeof(B_SEA_Controller_KIPPC_T));

  // states (dwork)
  (void) memset((void *)&SEA_Controller_KIPPC_DW, 0,
                sizeof(DW_SEA_Controller_KIPPC_T));

  {
    real_T Constant;
    static const char_T tmp[21] = { '/', 'e', 'l', 'a', 's', 't', 'i', 'c', '/',
      'j', 'o', 'i', 'n', 't', '_', 's', 't', 'a', 't', 'e', 's' };

    static const char_T tmp_0[15] = { 's', 'e', 'a', '_', 't', 'e', 't', 'a',
      'l', 'r', 'e', 'f', 'd', 'e', 'g' };

    static const char_T tmp_1[16] = { '/', 's', 'p', '/', 'j', 'o', 'i', 'n',
      't', '_', 's', 't', 'a', 't', 'e', 's' };

    char_T tmp_2[16];
    char_T tmp_3[17];
    int32_T i;

    // Start for Constant: '<S4>/Constant'
    Constant = SEA_Controller_KIPPC_P.Constant_Value_o;

    // Start for Atomic SubSystem: '<S3>/Publish'
    // Start for MATLABSystem: '<S59>/SinkBlock'
    SEA_Controller_KIPPC_DW.obj_g.isInitialized = 0;
    SEA_Controller_KIPPC_DW.obj_g.isInitialized = 1;
    for (i = 0; i < 16; i++) {
      tmp_3[i] = tmp_1[i];
    }

    tmp_3[16] = '\x00';
    Pub_SEA_Controller_KIPPC_15.createPublisher(tmp_3,
      SEA_Controller__MessageQueueLen);

    // End of Start for MATLABSystem: '<S59>/SinkBlock'
    // End of Start for SubSystem: '<S3>/Publish'

    // Start for Atomic SubSystem: '<S2>/Subscribe1'
    // Start for MATLABSystem: '<S54>/SourceBlock'
    SEA_Controller_KIPPC_DW.obj.isInitialized = 0;
    SEA_Controller_KIPPC_DW.obj.isInitialized = 1;
    for (i = 0; i < 15; i++) {
      tmp_2[i] = tmp_0[i];
    }

    tmp_2[15] = '\x00';
    Sub_SEA_Controller_KIPPC_35.createSubscriber(tmp_2,
      SEA_Controller__MessageQueueLen);

    // End of Start for MATLABSystem: '<S54>/SourceBlock'
    // End of Start for SubSystem: '<S2>/Subscribe1'

    // Start for Atomic SubSystem: '<S2>/Subscribe'
    // Start for MATLABSystem: '<S53>/SourceBlock'
    SEA_Controller_KIPPC_DW.obj_p.isInitialized = 0;
    SEA_Controller_KIPPC_DW.obj_p.isInitialized = 1;
    for (i = 0; i < 21; i++) {
      SEA_Controller_KIPPC_B.cv0[i] = tmp[i];
    }

    SEA_Controller_KIPPC_B.cv0[21] = '\x00';
    Sub_SEA_Controller_KIPPC_14.createSubscriber(SEA_Controller_KIPPC_B.cv0,
      SEA_Controller__MessageQueueLen);

    // End of Start for MATLABSystem: '<S53>/SourceBlock'
    // End of Start for SubSystem: '<S2>/Subscribe'

    // InitializeConditions for Delay: '<S6>/MemoryX'
    SEA_Controller_KIPPC_DW.icLoad = 1U;

    // InitializeConditions for DiscreteIntegrator: '<S4>/Discrete-Time Integrator' 
    SEA_Controller_KIPPC_DW.DiscreteTimeIntegrator_DSTATE = Constant;

    // SystemInitialize for Enabled SubSystem: '<S2>/Reference sensing'
    // SystemInitialize for Outport: '<S52>/tetalrefdeg'
    SEA_Controller_KIPPC_B.DataTypeConversion =
      SEA_Controller_KIPPC_P.tetalrefdeg_Y0;

    // End of SystemInitialize for SubSystem: '<S2>/Reference sensing'

    // SystemInitialize for Enabled SubSystem: '<S2>/Joint sensing'
    // SystemInitialize for Outport: '<S51>/tetamrad'
    SEA_Controller_KIPPC_B.pos_motor = SEA_Controller_KIPPC_P.tetamrad_Y0;

    // SystemInitialize for Outport: '<S51>/tetalrad'
    SEA_Controller_KIPPC_B.Sum1 = SEA_Controller_KIPPC_P.tetalrad_Y0;

    // End of SystemInitialize for SubSystem: '<S2>/Joint sensing'

    // SystemInitialize for Enabled SubSystem: '<S25>/MeasurementUpdate'
    // SystemInitialize for Outport: '<S50>/L*(y[k]-yhat[k|k-1])'
    SEA_Controller_KIPPC_B.Product3[0] = SEA_Controller_KIPPC_P.Lykyhatkk1_Y0;
    SEA_Controller_KIPPC_B.Product3[1] = SEA_Controller_KIPPC_P.Lykyhatkk1_Y0;
    SEA_Controller_KIPPC_B.Product3[2] = SEA_Controller_KIPPC_P.Lykyhatkk1_Y0;
    SEA_Controller_KIPPC_B.Product3[3] = SEA_Controller_KIPPC_P.Lykyhatkk1_Y0;

    // End of SystemInitialize for SubSystem: '<S25>/MeasurementUpdate'
  }
}

// Model terminate function
void SEA_Controller_KIPPC_terminate(void)
{
  // Terminate for Atomic SubSystem: '<S3>/Publish'
  // Start for MATLABSystem: '<S59>/SinkBlock' incorporates:
  //   Terminate for MATLABSystem: '<S59>/SinkBlock'

  if (SEA_Controller_KIPPC_DW.obj_g.isInitialized == 1) {
    SEA_Controller_KIPPC_DW.obj_g.isInitialized = 2;
  }

  // End of Start for MATLABSystem: '<S59>/SinkBlock'
  // End of Terminate for SubSystem: '<S3>/Publish'

  // Terminate for Atomic SubSystem: '<S2>/Subscribe1'
  // Start for MATLABSystem: '<S54>/SourceBlock' incorporates:
  //   Terminate for MATLABSystem: '<S54>/SourceBlock'

  if (SEA_Controller_KIPPC_DW.obj.isInitialized == 1) {
    SEA_Controller_KIPPC_DW.obj.isInitialized = 2;
  }

  // End of Start for MATLABSystem: '<S54>/SourceBlock'
  // End of Terminate for SubSystem: '<S2>/Subscribe1'

  // Terminate for Atomic SubSystem: '<S2>/Subscribe'
  // Start for MATLABSystem: '<S53>/SourceBlock' incorporates:
  //   Terminate for MATLABSystem: '<S53>/SourceBlock'

  if (SEA_Controller_KIPPC_DW.obj_p.isInitialized == 1) {
    SEA_Controller_KIPPC_DW.obj_p.isInitialized = 2;
  }

  // End of Start for MATLABSystem: '<S53>/SourceBlock'
  // End of Terminate for SubSystem: '<S2>/Subscribe'
}

//
// File trailer for generated code.
//
// [EOF]
//
