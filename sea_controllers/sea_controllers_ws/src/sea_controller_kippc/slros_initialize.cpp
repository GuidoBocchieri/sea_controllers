#include "slros_initialize.h"

ros::NodeHandle * SLROSNodePtr;
const std::string SLROSNodeName = "SEA_Controller_KIPPC";

// For Block SEA_Controller_KIPPC/Input Interface/Subscribe
SimulinkSubscriber<sensor_msgs::JointState, SL_Bus_SEA_Controller_KIPPC_sensor_msgs_JointState> Sub_SEA_Controller_KIPPC_14;

// For Block SEA_Controller_KIPPC/Input Interface/Subscribe1
SimulinkSubscriber<std_msgs::Float32, SL_Bus_SEA_Controller_KIPPC_std_msgs_Float32> Sub_SEA_Controller_KIPPC_35;

// For Block SEA_Controller_KIPPC/Output Interface/Publish
SimulinkPublisher<sensor_msgs::JointState, SL_Bus_SEA_Controller_KIPPC_sensor_msgs_JointState> Pub_SEA_Controller_KIPPC_15;

void slros_node_init(int argc, char** argv)
{
  ros::init(argc, argv, SLROSNodeName);
  SLROSNodePtr = new ros::NodeHandle();
}

