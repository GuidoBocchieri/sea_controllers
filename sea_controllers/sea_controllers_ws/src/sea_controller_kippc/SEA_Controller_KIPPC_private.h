//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
//
// File: SEA_Controller_KIPPC_private.h
//
// Code generated for Simulink model 'SEA_Controller_KIPPC'.
//
// Model version                  : 1.12
// Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
// C/C++ source code generated on : Tue Jul 11 13:57:24 2017
//
// Target selection: ert.tlc
// Embedded hardware selection: Generic->Unspecified (assume 32-bit Generic)
// Code generation objectives: Unspecified
// Validation result: Not run
//
#ifndef RTW_HEADER_SEA_Controller_KIPPC_private_h_
#define RTW_HEADER_SEA_Controller_KIPPC_private_h_
#include "rtwtypes.h"
#endif                                 // RTW_HEADER_SEA_Controller_KIPPC_private_h_ 

//
// File trailer for generated code.
//
// [EOF]
//
